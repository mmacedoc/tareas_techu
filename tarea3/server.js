var express=require('express'); //referencia al paquete express
require('dotenv').config();

const userFile=require('./data/users.json');
const bodyParser=require("body-parser");
var app=express();
app.use(bodyParser.json());
const PORT=3000;
const URL_BASE="/api-peru/v1/";

app.listen(PORT,function(){
  console.log('node js esuchando en el puerto 3000...');
});

//operacion get todos los usuarios usr.json
app.get(URL_BASE+"users",
     function(request,response){
       response.status(200);
       response.send(userFile);
     });

 //get a un uncio usuario con ID
      app.get(URL_BASE+"users/:id/:id2/:id3",
           function(request,response){
             let indice =request.params.id;
               let indice2 =request.params.id2;
                 let indice3 =request.params.id3;
             console.log("valor buscado "+indice);
               console.log("valor buscado 2 "+indice2);
               console.log("valor buscado 3 "+indice3);
             response.status(200);
             response.send(userFile[indice-1]);
           });


  //busqueda por llave
   app.get(URL_BASE+"users/:id",
        function(request,response){
          let indice =request.params.id;
         let respuesta=
          (userFile[indice-1]==undefined)?{"msg":"No Existe"}:userFile[indice-1];
          response.status(200);
          response.send(respuesta);
        });

//tarea2  para la casa, actualizar
          app.put(URL_BASE + 'users/:id',
              function(request,response) {
                let indice = request.params.id;
                var msg;
                if (userFile[indice-1] == undefined){
                    msg = {"msg":"No existe"} ;
                  } else {
                   let user = {
                   id: + indice,
                   first_name: request.body.first_name,
                   last_name: request.body.last_name,
                   email: request.body.email,
                   password: request.body.password
                 }

                userFile.splice(indice-1,1,user);
                msg = {"usuario actualizado":user,
                     "userfile es":userFile};
              }
                response.status(200).send(msg);
          });



// LOGIN - user.json
app.post(URL_BASE + 'login',
 function(request, response) {
 // console.log("POST /apitechu/v1/login");
  console.log(request.body.email);
  console.log(request.body.password);
  var user = request.body.email;
  var pass = request.body.password;
  for(us of userFile) {
   if(us.email == user) {
    if(us.password == pass) {
     us.logged = true;
     writeUserDataToFile(userFile);
     console.log("Login correcto!");
     response.send({"msg" : "Login correcto.",
             "idUsuario" : us.usuarioId,
              "logged" : "true"});
    } else {
     console.log("Login incorrecto.");
     response.send({"msg" : "Login incorrecto."});
    }
   }
  }
});

// LOGOUT - users.json
app.post(URL_BASE + 'logout',
 function(request, response) {
  //console.log("POST /apitechu/v1/logout");
  var userId = request.body.usuarioId;
  for(us of userFile) {
   if(us.usuarioId == userId) {
    if(us.logged) {
     delete us.logged; // borramos propiedad 'logged'
     writeUserDataToFile(userFile);
     console.log("Logout correcto!");
     response.send({"msg" : "Logout correcto.", "idUsuario" : us.usuarioId});
    } else {
     console.log("Logout incorrecto.");
     response.send({"msg" : "Logout incorrecto."});
    }
   }
  }
});


function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
     console.log(err);
    } else {
     console.log("Datos escritos en 'user.json'.");
    }
   })
 }
